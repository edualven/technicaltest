<?php

namespace App\Controller;

use App\Repository\PositionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route (path="/api/")
 */
class PositionController extends AbstractController
{
    /**
     * @Route ("position/{id}/players", name="players_by_position", methods={"GET"})
     */
    public function getPlayersByPosition(
        Request            $request,
        PositionRepository $positionRepository,
        PlayerController $playerController
    ): JsonResponse
    {
        $response = new JsonResponse();
        $position_id = $request->get('id');
        $position = $positionRepository->findOneBy(['id' => $position_id]);

        if (!empty($position)) {
            $players = $position->getPlayers()->toArray();
            $players_as_array = $playerController->formatDataToReturn($players);

            return $response->setData(array(
                    'success' => true,
                    'data' => $players_as_array
                )
            );
        }

        $response->setStatusCode(404);
        $response->setData(array(
                'success' => false,
                'error' => 'The position id not exists'
            )
        );

        return $response;
    }
}
