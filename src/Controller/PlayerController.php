<?php

namespace App\Controller;

use App\Entity\Player;
use App\Repository\PlayerRepository;
use App\Repository\PositionRepository;
use App\Repository\TeamRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route (path="/api/")
 */
class PlayerController extends AbstractController
{
    /**
     * @Route ("players", name="players_list")
     * @param PlayerRepository $playerRepository
     * @return JsonResponse
     */
    public function getAllPlayers(PlayerRepository $playerRepository): JsonResponse
    {
        $response = new JsonResponse();
        $players = $playerRepository->findAll();
        $players_as_array = $this->formatDataToReturn($players);
        return $response->setData(array(
                'success' => true,
                'data' => $players_as_array
            )
        );
    }

    /**
     * @Route ("player", name="create_player", methods={"POST"})
     * @param Request $request
     * @param PlayerRepository $playerRepository
     * @param TeamRepository $teamRepository
     * @return JsonResponse
     */
    public function addPlayer(
        Request            $request,
        PlayerRepository   $playerRepository,
        TeamRepository     $teamRepository,
        PositionRepository $positionRepository
    ): JsonResponse
    {
        $response = new JsonResponse();
        $array_error_messages = array();
        $data = json_decode($request->getContent(), true);
        $name = $data['name'];
        $price = (float)$data['price'];
        $team_id = (int)$data['team'];
        $position_name = $data['position'];

        $team = $teamRepository->findOneBy(['id' => $team_id]);
        $position = $positionRepository->findOneBy(['name' => $position_name]);
        if (empty($name)) {
            $array_error_messages[] = 'The name cannot be empty';
        }
        if (empty($position)) {
            $array_error_messages[] = 'The position entered is incorrect';
        }
        if (empty($team)) {
            $array_error_messages[] = 'The team sent does not exist';
        }
        if (!empty($name) && !empty($position) && !empty($team)) {
            $player = new Player();
            $player->setName($name);
            $player->setPrice($price);
            $player->setTeam($team);
            $player->setPosition($position);

            $playerRepository->add($player);
            $response->setData(array(
                    'success' => true,
                    'data' => array(
                        'id' => $player->getId(),
                        'name' => $player->getName()
                    )
                )
            );
            return $response;
        }

        $response->setStatusCode(400);
        $response->setData(array(
                'success' => false,
                'error' => $array_error_messages,
                'data' => null
            )
        );
        return $response;
    }

    /**
     * @Route("player/{id}", name="delete_player", methods={"DELETE"})
     * @param Request $request
     * @param PlayerRepository $playerRepository
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deletePlayer(Request $request, PlayerRepository $playerRepository): JsonResponse
    {
        $response = new JsonResponse();
        $player_id = $request->get('id');
        $player = $playerRepository->findOneBy(['id' => $player_id]);
        if (!empty($player)) {
            $response->setData(array(
                    'success' => true,
                    'message' => 'Player ' . $player->getName() . ' has been deleted'
                )
            );
            $playerRepository->remove($player);
            return $response;
        }

        $response->setStatusCode(404);
        $response->setData(array(
                'success' => false,
                'error' => 'The player id not exists'
            )
        );

        return $response;
    }

    /**
     * @Route("player/{id}", name="update_player", methods={"PUT","PATCH"})
     * @param Request $request
     * @param PlayerRepository $playerRepository
     * @param TeamRepository $teamRepository
     * @param PositionRepository $positionRepository
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updatePlayer(
        Request            $request,
        PlayerRepository   $playerRepository,
        TeamRepository     $teamRepository,
        PositionRepository $positionRepository
    ): JsonResponse
    {
        $response = new JsonResponse();
        $array_error_messages = array();
        $player_id = $request->get('id');
        $player = $playerRepository->findOneBy(['id' => $player_id]);

        $data = json_decode($request->getContent(), true);
        $name = (!empty($data['name'])) ? $data['name'] : null;
        $price = (!empty($data['price'])) ? $data['price'] : null;
        $team_id = (!empty($data['team'])) ? $data['team'] : null;
        $position_name = (!empty($data['position'])) ? $data['position'] : null;

        $team = $teamRepository->findOneBy(['id' => $team_id]);
        $position = $positionRepository->findOneBy(['name' => $position_name]);
        if (empty($price)) {
            $array_error_messages[] = 'The price cannot be empty';
        }
        if (empty($player)) {
            $array_error_messages[] = 'The player id sent does not exist or cannot be empty';
        }
        if (empty($name)) {
            $array_error_messages[] = 'The name cannot be empty';
        }
        if (empty($position)) {
            $array_error_messages[] = 'The position entered is incorrect or cannot be empty';
        }
        if (empty($team)) {
            $array_error_messages[] = 'The team id sent does not exist or cannot be empty';
        }
        if (empty($array_error_messages)) {
            $player->setName($name);
            $player->setPrice($price);
            $player->setTeam($team);
            $player->setPosition($position);

            $playerRepository->add($player);
            $response->setData(array(
                    'success' => true,
                    'data' => array(
                        'id' => $player->getId(),
                        'name' => $player->getName()
                    )
                )
            );
            return $response;
        }

        $response->setStatusCode(400);
        $response->setData(array(
                'success' => false,
                'error' => $array_error_messages,
                'data' => null
            )
        );
        return $response;
    }

    /**
     * @Route ("player/{id}", name="get_player", methods={"GET"})
     * @param Request $request
     * @param PlayerRepository $playerRepository
     * @return JsonResponse
     */
    public function getPlayer(Request $request, PlayerRepository $playerRepository): JsonResponse
    {
        $response = new JsonResponse();
        $player_id = $request->get('id');
        $player = $playerRepository->findBy(['id' => $player_id]);
        $player_as_array = $this->formatDataToReturn($player);
        if (!empty($player)) {
            return $response->setData(array(
                    'success' => true,
                    'data' => $player_as_array
                )
            );
        }

        $response->setStatusCode(404);
        $response->setData(array(
                'success' => false,
                'error' => 'The player id not exists'
            )
        );

        return $response;
    }

    /**
     * @Route("players/team/{idt}/position/{idp}")
     * @param Request $request
     * @param PlayerRepository $playerRepository
     * @return JsonResponse|void
     */
    public function getPlayersByTeamAndPosition(
        Request          $request,
        PlayerRepository $playerRepository
    )
    {
        $response = new JsonResponse();
        $team_id = (int)$request->get('idt');
        $position_id = (int)$request->get('idp');
        $players = $playerRepository->findBy(['team' => $team_id, 'position' => $position_id]);

        $player_as_array = $this->formatDataToReturn($players);
        if (!empty($players)) {
            return $response->setData(array(
                    'success' => true,
                    'data' => $player_as_array
                )
            );
        }

        $response->setStatusCode(404);
        $response->setData(array(
                'success' => false,
                'error' => 'There are no players of the indicated team and position'
            )
        );

        return $response;
    }

    /**
     * @Route("player/{id}/price/{symbol}", name="player_price_currecy", methods={"GET"})
     * @param Request $request
     * @param PlayerRepository $playerRepository
     * @return JsonResponse
     */
    public function getPlayerPriceCurrency(Request $request, PlayerRepository $playerRepository): JsonResponse
    {
        $response = new JsonResponse();
        $player_id = $request->get('id');
        $symbol = $request->get('symbol');
        $exchange_data = $this->exchangeGenerate($symbol);
        $player = $playerRepository->findBy(['id' => $player_id]);

        if (!empty($player) && isset($exchange_data['success']) && $exchange_data['success']) {
            $player_as_array = $this->formatDataToReturn($player, $symbol, $exchange_data['rates'][$symbol]);
            return $response->setData(array(
                    'success' => true,
                    'data' => $player_as_array
                )
            );
        }

        $response->setStatusCode(404);
        $response->setData(array(
                'success' => false,
                'error' => 'The player id or currency is wrong'
            )
        );

        return $response;
    }

    /**
     * @param array $data
     * @param string|null $symbol
     * @param float|null $currency_value
     * @return array
     */
    public function formatDataToReturn(array $data, string $symbol = null, float $currency_value = null): array
    {
        $players_as_array = array();
        foreach ($data as $player) {
            $player_data = array(
                'id' => $player->getId(),
                'name' => $player->getName(),
                'position' => $player->getPosition()->getName(),
                'team' => $player->getTeam()->getName(),
                'price' => $player->getPrice()
            );
            if (!empty($symbol) && !empty($currency_value)) {
                $player_data['price_currency'] = number_format($player->getPrice() * $currency_value, 2) . ' ' . $symbol;
            }
            $players_as_array [] = $player_data;
        }
        return $players_as_array;
    }

    /**
     * @return mixed
     */
    public function exchangeGenerate($symbol = 'USD')
    {
        $endpoint = 'latest';
        $base_url = 'http://api.exchangeratesapi.io/v1/';
        $access_key = $this->getParameter('app.exchange_generate_access_key');

        $ch = curl_init($base_url . $endpoint . '?access_key=' . $access_key . '&symbols=' . $symbol);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $data = curl_exec($ch);
        curl_close($ch);

        return json_decode($data, true);
    }
}
