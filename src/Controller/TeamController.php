<?php

namespace App\Controller;

use App\Entity\Team;
use App\Repository\TeamRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route (path="/api/")
 */
class TeamController extends AbstractController
{
    /**
     * @Route ("teams",name="get_all_teams",methods={"GET"})
     * @param TeamRepository $teamRepository
     * @return JsonResponse
     */
    public function getAllTeams(TeamRepository $teamRepository): JsonResponse
    {
        $teams = $teamRepository->findAll();
        $team_as_array = array();
        foreach ($teams as $team) {
            $team_as_array[] = array(
                'id' => $team->getId(),
                'name' => $team->getName()
            );
        }
        $response = new JsonResponse();
        $response->setData(array(
                'success' => true,
                'data' => $team_as_array
            )
        );
        return $response;
    }

    /**
     * @Route("team/{id}", name="get_team", methods={"GET"})
     * @param Request $request
     * @param TeamRepository $teamRepository
     * @return JsonResponse
     */
    public function getTeam(Request $request, TeamRepository $teamRepository): JsonResponse
    {
        $response = new JsonResponse();
        $team_id = (int)$request->get('id');
        $team = $teamRepository->findOneBy(['id' => $team_id]);
        if (!empty($team)) {
            return $response->setData(array(
                    'success' => true,
                    'data' => array(
                        'id' => $team->getId(),
                        'name' => $team->getName()
                    )
                )
            );
        }

        $response->setStatusCode(404);
        $response->setData(array(
                'success' => false,
                'error' => 'The team id not exists'
            )
        );

        return $response;

    }

    /**
     * @Route ("team",name="create_team",methods={"POST"})
     * @param Request $request
     * @param TeamRepository $teamRepository
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addTeam(Request $request, TeamRepository $teamRepository): JsonResponse
    {
        $response = new JsonResponse();
        $data = json_decode($request->getContent(), true);
        $name = $data['name'];
        if (!empty($name)) {
            $team = new Team();
            $team->setName($name);
            $teamRepository->add($team);
            $response->setData(array(
                    'success' => true,
                    'data' => array(
                        'id' => $team->getId(),
                        'name' => $team->getName()
                    )
                )
            );
            return $response;
        }

        return $response->setData(array(
                'success' => false,
                'error' => 'Team name cannot be empty',
                'data' => null
            )
        );
    }

    /**
     * @Route ("team/{id}",name="delete_team",methods={"DELETE"})
     * @param Request $request
     * @param TeamRepository $teamRepository
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteTeam(Request $request, TeamRepository $teamRepository): JsonResponse
    {
        $response = new JsonResponse();
        $team_id = $request->get('id');
        $team = $teamRepository->findOneBy(['id' => $team_id]);
        if (!empty($team)) {
            $response->setData(array(
                    'success' => true,
                    'message' => 'Team ' . $team->getName() . ' has been deleted'
                )
            );
            $teamRepository->remove($team);
            return $response;
        }

        $response->setData(array(
                'success' => false,
                'error' => 'The team id not exists'
            )
        );

        return $response;
    }

    /**
     * @Route ("team/{id}",name="update_team",methods={"PUT","PATCH"})
     * @param Request $request
     * @param TeamRepository $teamRepository
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateTeam(Request $request, TeamRepository $teamRepository): JsonResponse
    {
        $response = new JsonResponse();
        $team_id = $request->get('id');
        $team = $teamRepository->findOneBy(['id' => $team_id]);

        $data = json_decode($request->getContent(), true);
        $name = $data['name'];
        if (!empty($team) && !empty($name)) {
            $team->setName($name);
            $teamRepository->add($team);
            $response->setData(array(
                    'success' => true,
                    'message' => 'Team id:' . $team_id . ' updated'
                )
            );
            return $response;
        }

        $response->setData(array(
                'success' => false,
                'error' => 'The team id not exists or name is empty'
            )
        );

        return $response;
    }

    /**
     * @Route("team/{id}/players", name="get_players_by_team", methods={"GET"})
     * @param Request $request
     * @param TeamRepository $teamRepository
     * @param PlayerController $playerController
     * @return JsonResponse
     */
    public function getPlayerByTeam(
        Request          $request,
        TeamRepository   $teamRepository,
        PlayerController $playerController
    ): JsonResponse
    {
        $response = new JsonResponse();
        $team_id = $request->get('id');
        $team = $teamRepository->findOneBy(['id' => $team_id]);

        if (!empty($team)) {
            $players = $team->getPlayers()->toArray();
            $players_as_array = $playerController->formatDataToReturn($players);

            return $response->setData(array(
                    'success' => true,
                    'data' => $players_as_array
                )
            );
        }

        $response->setStatusCode(404);
        $response->setData(array(
                'success' => false,
                'error' => 'The team id not exists'
            )
        );

        return $response;
    }
}